package by.gsu.igi.students.StarovoitovaPolina;

import java.util.Scanner;

public class RomaMolodez {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("inter first number: ");
        int firstNumber = scanner.nextInt();
        System.out.println("inter second number: ");
        int secondNumber = scanner.nextInt();

        int amount = firstNumber + secondNumber;
        System.out.println(firstNumber + " + " + secondNumber + " = " + amount);

        int difference = firstNumber - secondNumber;
        System.out.println(firstNumber + " - " + secondNumber + " = " + difference);

        int multiplication = firstNumber * secondNumber;
        System.out.println(firstNumber + " * " + secondNumber + " = " + multiplication);

        if (firstNumber == 0 || secondNumber == 0) {
            System.out.println("Division by zero");
        } else {
            int division = firstNumber / secondNumber;
            System.out.println(firstNumber + " / " + secondNumber + " = " + division);
        }

        int mod = firstNumber % secondNumber;
        System.out.println(firstNumber + " mod " + secondNumber + " = " + mod);
    }
}
