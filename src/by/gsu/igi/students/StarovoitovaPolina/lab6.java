package by.gsu.igi.students.StarovoitovaPolina;

public class lab6 {
    public static void main(String[] args) {

        String[] names = {

                "Елена", "Дмитрий", "Сергей", "Андрей",
                "Ольга", "Виктория", "Юлия", "Эмма",
                "Алексей", "Вероника", "Анна", "Вадим",
                "Тимофей", "Иван", "Екатерина", "Павел"

        };

        int[] times = {

                341, 273, 278, 329, 445, 402, 388, 275,

                243, 334, 412, 393, 299, 343, 317, 265

        };

        for (int i = 0; i < names.length; i++) {

            System.out.println(names[i] + ":" + times[i]);

        }

        System.out.println("\nСамый быстрый бегун\n" + names[idMin1Time(times)] + ":" + times[idMin1Time(times)]);

        System.out.println("\nВторой быстрый бегун\n" + names[idMin2Time(times)] + ":" + times[idMin2Time(times)]);

    }

    public static int idMin1Time(int[] massive) {
        int min = massive[0], idMin = -1;

        for (int i = 0; i < massive.length; i++) {

            if (massive[i] < min) {

                idMin = i;

                min = massive[i];

            }

        }

        return idMin;

    }

    public static int idMin2Time(int[] massive) {

        int min = massive[0], idMin = -1;

        for (int i = 0; i < massive.length; i++) {

            if (massive[i] < min && massive[i] > massive[idMin1Time(massive)]) {

                idMin = i;

                min = massive[i];

            }

        }

        return idMin;

    }

}

