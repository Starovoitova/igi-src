package by.gsu.igi.students.StarovoitovaPolina;

import java.util.Scanner;

public class Radius {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("R = ");
        double radius = scanner.nextByte();

        double perimeter = 2 * Math.PI * radius;
        System.out.println("P = " + perimeter);

        double area = Math.PI * Math.pow(radius, 2);
        System.out.println("S = " + area);
    }
}

